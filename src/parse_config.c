#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <yaml.h>

#include "parse_config.h"

static void state_start(struct config *config, yaml_event_t event)
{
	switch (event.type) {
	case YAML_STREAM_START_EVENT:
		config->state = STATE_STREAM;
		break;
	default:
		fprintf(stderr, "Error: unexpected event %d in state %d.\n",
			event.type, config->state);
		exit(1);
		break;
	}

	return;
}

static void state_stream(struct config *config, yaml_event_t event)
{
	switch (event.type) {
	case YAML_DOCUMENT_START_EVENT:
		config->state = STATE_DOCUMENT;
		break;
	case YAML_DOCUMENT_END_EVENT:
		config->state = STATE_STOP;
		break;
	default:
		fprintf(stderr, "Error: unexpected event %d in state %d.\n",
			event.type, config->state);
		exit(1);
		break;
	}

	return;
}

static void state_document(struct config *config, yaml_event_t event)
{
	switch (event.type) {
	case YAML_MAPPING_START_EVENT:
		config->state = STATE_SECTION;
		break;
	case YAML_MAPPING_END_EVENT:
		config->state = STATE_STREAM;
		break;
	default:
		fprintf(stderr, "Error: unexpected event %d in state %d.\n",
			event.type, config->state);
		exit(1);
		break;
	}

	return;
}

static void state_section(struct config *config, yaml_event_t event)
{
	char *value;

	switch (event.type) {
	case YAML_SCALAR_EVENT:
		value = (char *)event.data.scalar.value;
		if (0 == strcmp(value, "disks")) {
			config->state = STATE_DISK;
		} else if (0 == strcmp(value, "portage")) {
			config->state = STATE_PORTAGE;
		} else if (0 == strcmp(value, "locale")) {
			config->state = STATE_LOCALE;
		} else if (0 == strcmp(value, "net")) {
			config->state = STATE_NET;
		} else if (0 == strcmp(value, "users")) {
			config->state = STATE_USERS_LIST;
		} else {
			fprintf(stderr,
				"Error: unexpected event %d in state %d.\n",
				event.type, config->state);
			exit(1);
		}
		break;

	case YAML_DOCUMENT_END_EVENT:
		config->state = STATE_STREAM;
		break;

	default:
		fprintf(stderr, "Error: unexpected event %d in state %d.\n",
			event.type, config->state);
		exit(1);
		break;
	}

	return;
}

static void state_disk(struct config *config, yaml_event_t event)
{
	switch (event.type) {
	case YAML_MAPPING_START_EVENT:
		config->state = STATE_DISK_ROOT_FS;
		break;

	case YAML_MAPPING_END_EVENT:
		config->state = STATE_SECTION;
		break;

	default:
		fprintf(stderr, "Error: unexpected event %d in state %d.\n",
			event.type, config->state);
		exit(1);
		break;
	}

	return;
}

static void state_disk_root_fs(struct config *config, yaml_event_t event)
{
	char *value;

	switch (event.type) {
	case YAML_SCALAR_EVENT:
		value = (char *)event.data.scalar.value;
		if (0 == strcmp(value, "root_fs")) {
			config->state = STATE_DISK_ROOT_FS_KEY;
		} else {
			fprintf(stderr, "Error, unknown scalar %s\n", value);
			exit(1);
		}
		break;

	default:
		fprintf(stderr, "Error: unexpected event %d in state %d.\n",
			event.type, config->state);
		exit(1);
		break;
	}

	return;
}

static void state_disk_root_fs_key(struct config *config, yaml_event_t event)
{
	char *value;

	switch (event.type) {
	case YAML_SCALAR_EVENT:
		value = (char *)event.data.scalar.value;
		if (0 == strcmp(value, "xfs")) {
			strcpy(config->disk.rootfs, "xfs");
		} else if (0 == strcmp(value, "ext4")) {
			strcpy(config->disk.rootfs, "ext4");
		} else {
			fprintf(stderr, "Error, unknown scalar %s\n", value);
			exit(1);
		}
		break;

	case YAML_MAPPING_END_EVENT:
		config->state = STATE_STREAM;
		break;

	default:
		fprintf(stderr, "Error: unexpected event %d in state %d.\n",
			event.type, config->state);
		exit(1);
		break;
	}

	return;
}

/* Open the config file */
FILE *open_config_file(const char *config_file)
{
	FILE *fptr;
	fptr = fopen(config_file, "r");

	if (fptr == NULL) {
		fprintf(stderr, "Can't open %s\n", config_file);
		exit(1);
	}

	return fptr;
}

/* Parse our config file from yaml to struct */
void read_config_file(const char *config_file, struct config *config)
{
	FILE *fptr = open_config_file(config_file);

	yaml_parser_t parser;
	yaml_event_t event;
	if (!yaml_parser_initialize(&parser)) {
		fprintf(stderr, "Error initializing the parser\n");
		exit(1);
	}

	yaml_parser_set_input_file(&parser, fptr);

	do {
		yaml_parser_parse(&parser, &event);
		if (parser.problem) {
			fprintf(stderr, "YAML parsing error: %s\n",
				parser.problem);
			exit(1);
			break;
		}

		switch (config->state) {
		case STATE_START:
			state_start(config, event);
			break;
		case STATE_STREAM:
			state_stream(config, event);
			break;

		case STATE_DOCUMENT:
			state_document(config, event);
			break;

		case STATE_SECTION:
			state_section(config, event);
			break;

		case STATE_DISK:
			state_disk(config, event);
			break;

		case STATE_DISK_ROOT_FS:
			state_disk_root_fs(config, event);
			break;

		case STATE_DISK_ROOT_FS_KEY:
			state_disk_root_fs_key(config, event);

			/* Temporary way to return with incomplete data */
			return;
			break;

		default:
			fprintf(stderr, "I do not handle %d yet\n", event.type);
			exit(1);
		}

		if (event.type != YAML_STREAM_END_EVENT)
			yaml_event_delete(&event);
	} while (event.type != YAML_STREAM_END_EVENT);

	yaml_event_delete(&event);
	yaml_parser_delete(&parser);
	fclose(fptr);
	fptr = NULL;

	return;
}
