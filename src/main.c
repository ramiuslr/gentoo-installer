#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include "main.h"
#include "parse_config.h"

static void show_usage(void)
{
	printf("USAGE:\n\tgi [path/config.yml] [ip address] [port]\n");
	exit(0);

	return;
}

int main(int argc, char **argv)
{
	if (4 != argc)
		show_usage();

	const char *config_file_path = argv[1];

	/* Commented for later use */
	/* const char *target_machine_address = argv[2]; */
	/* const int target_machine_port = atoi(argv[3]); */

	struct config *config = malloc(sizeof(*config));
	if (NULL == config) {
		fprintf(stderr, "Error while allocating memory for config\n");
	}

	read_config_file(config_file_path, config);

	/* Temporary check for config fields */
	printf("Root FS: %s\n", config->disk.rootfs);

	free(config);
	config = NULL;

	return 0;
}
