#ifndef PARSE_CONFIG_H
#define PARSE_CONFIG_H

#include <stdio.h>
#include <yaml.h>

enum state {
	STATE_START,
	STATE_STREAM,
	STATE_DOCUMENT,
	STATE_SECTION,

	STATE_DISK,
	STATE_DISK_ROOT_FS,
	STATE_DISK_ROOT_FS_KEY,

	STATE_PORTAGE,
	STATE_PORTAGE_MAKECONF,
	STATE_PORTAGE_MAKECONF_COMMONFLAGS,
	STATE_PORTAGE_MAKECONF_USE,
	STATE_PORTAGE_MAKECONF_L10N,
	STATE_PORTAGE_MAKECONF_VIDEOCARDS,
	STATE_PORTAGE_MAKECONF_INPUT_DEVICES,
	STATE_PORTAGE_MAKECONF_MIRRORS,
	STATE_PORTAGE_PACKAGE_USE_LIST,
	STATE_PORTAGE_PACKAGE_LICENSE_LIST,

	STATE_LOCALE,
	STATE_LOCALE_LANGUAGE,
	STATE_LOCALE_TIMEZONE,
	STATE_LOCALE_KEYMAP,

	STATE_NET,
	STATE_NET_NIC,
	STATE_NET_CONFIG,
	STATE_NET_ADDR,
	STATE_NET_ROUTE,
	STATE_NET_DNS,

	STATE_USERS_LIST,
	STATE_USERS_USER,
	STATE_USERS_USER_GROUPS_LIST,
	STATE_USERS_USER_SHELL,

	STATE_BOOTLOADER,

	STATE_STOP
};

struct config {
	enum state state;
	struct {
		char rootfs[12];
	} disk;
	struct {
		char common_flags[1024];
		char use[1024];
		char l10n[8];
		char video_cards[1024];
		char input_devices[1024];
		char mirrors[8][2048];
	} portage;
	struct {
		char language[64];
		char timezone[64];
		char keymap[8];
	} locale;
	struct {
		char nic[16];
		char config[16];
		char addr[32];
		char route[32];
		char dns[32];
	} net;
	struct {
		char root_pwd[128];
		char root_shell[32];
		struct {
			char name[64];
			char pwd[128];
			char shell[64];
			char groups[16][32];
		} user;
	} users;
};

FILE *open_config_file(const char *config_file_path);
void read_config_file(const char *config_file_path, struct config *config);

#endif
