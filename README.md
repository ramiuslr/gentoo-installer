Gentoo Installer
===

# Purpose
The goal is to provide a fully automated and
as-code installer for the *Gentoo Linux* distribution.

# Project state
This project is just starting, as a hobby and a way to learn C programming.

# How to build
You will need the following libraries to build:
- `libyaml`
- `libssh`

Then this is as simple as:
```bash
make
```

If you wanna update `compile_commands.json` for `clang` linting:
```bash
make compile_commands
```

To format your code:
```bash
make format
```

To launch gdb on the program (Need to use debug CFLAG):
```bash
make debug
```

If you want to install it in your path (`/usr/local/bin`):
```bash
make install
```
To uninstall later:
```bash
make uninstall
```

# Usage
```bash
gi config.yaml 192.168.1.50 22 # Should be [config] [ip] [port]
```

# TODO
- [x] Use C language
- [ ] Be able to use different Linux live images for install (Gentoo, ArchLinux, Alpine, etc.)
- [ ] Use libssh to pass remote commands
- [x] Based on a config file with each installation options
- [ ] Should be able to use ZFS as a root filesystem
- [ ] Use zRAM on the installed system
- [ ] Use gentoo-kernel-bin
- [ ] Show realtime progress
- [ ] Command may look like:
- [ ] Prompt for SSH password
- [ ] Handle chroot properly

- [ ] Some parameters config file should have:
	- [ ] Partitionning scheme
	- [ ] Network config (live IP @ + post-install net config)
	- [ ] Packages to preinstall
	- [ ] System location settings
	- [ ] Gentoo make.conf settings, + mirrors
	- [ ] Configuration des utilisateurs
	- [ ] Bootloader:
		- [ ] Grub
		- [ ] EFI -> Kernel (EFI stub)

# LICENSE
This project is made available under the MIT license.
