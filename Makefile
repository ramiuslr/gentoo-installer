CC = clang

# RELEASE
# CFLAGS = -c -O2 -Wall -Wextra -Werror -pedantic -std=c11
# DEBUG
CFLAGS = -c -g -Wall -Wextra -Werror -pedantic -std=c11

INCLUDES = -I/usr/include
LIBS = -lssh -lyaml

SRC = $(shell find . -name "*.c")
OBJ = $(patsubst %.c, %.o, $(SRC))
TARGET = gi

all: clean $(TARGET)

%.o: %.c
	$(CC) $(CFLAGS) -o $@ $<

$(TARGET): $(OBJ)
	$(CC) -o $@ $^ $(INCLUDES) $(LIBS)

install:
	cp $(TARGET) /usr/local/bin/

uninstall:
	rm -f /usr/local/bin/$(TARGET)

clean:
	@find . -name "*.o" -exec rm -rf {} \;
	@rm -rf $(TARGET)

compile_commands:
	@bear -- make

format:
	@clang-format -i src/*.[ch]

debug:
	@gdb --args $(TARGET) test/config.yaml 192.168.1.50 22

test: all
	@printf "\n\n--- Starting test ---\n\n"
	@./gi test/config.yaml 192.168.1.50 22

.PHONY: clean install uninstall compile_commands format
